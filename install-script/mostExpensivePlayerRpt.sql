SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE proc [dbo].[sp_mostExpensivePlayer] 
@season varchar(20) = '2000 Regular' as
select top 1 p.player_name, contract_amt 
from contract c 
inner join player p on c.player_id = p.id 
inner join season s on s.id = c.season_id
order by contract_amt desc
GO
