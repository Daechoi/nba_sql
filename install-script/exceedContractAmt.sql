create proc sp_ExceededContractAmt as
SELECT t.team_name,t.max_contract_amt,sum(c.contract_amt) as contract_amt FROM
team t inner join contract c 
on t.id = c.team_id 
inner join season s on s.id = c.season_id
where s.season_name = '2000 Regular'
group by t.team_name, t.max_contract_amt
having sum(c.contract_amt) > (t.max_contract_amt)
