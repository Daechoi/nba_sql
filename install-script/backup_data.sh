source ./dotenv_shell_loader.sh
dotenv
bcp $DatabaseName.dbo.league out $SqlSourceDataPath/bak_league.csv -c -E -S $Server -U $username -P $password
bcp $DatabaseName.dbo.season out $SqlSourceDataPath/bak_season.csv -c -E -S $Server -U $username -P $password
bcp $DatabaseName.dbo.team out $SqlSourceDataPath/bak_team.csv -c -E -S $Server -U $username -P $password
bcp $DatabaseName.dbo.player out $SqlSourceDataPath/bak_player.csv -c -S $Server -U $username -P $password
bcp $DatabaseName.dbo.position out $SqlSourceDataPath/bak_position.csv -c -S $Server -U $username -P $password
bcp $DatabaseName.dbo.contract out $SqlSourceDataPath/bak_contract.csv -c -S $Server -U $username -P $password
