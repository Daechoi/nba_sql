source ./dotenv_shell_loader.sh
# Setup the environment variable including the username and password
dotenv
sqlcmd -S $Server -U $username -P $password -d $DatabaseName -i triggerReplication.sql
