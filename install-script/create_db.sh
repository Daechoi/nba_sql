source ./dotenv_shell_loader.sh
# Setup the environment variable including the username and password
dotenv
sqlcmd -S $Server -U $username -P $password -i create_hoopDB.sql
sqlcmd -S $Server -U $username -P $password -d $DatabaseName -i create_Tables.sql
sqlcmd -S $Server -U $username -P $password -d $DatabaseName -i create_storedprocs.sql
