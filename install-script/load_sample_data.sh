source ./dotenv_shell_loader.sh
dotenv
sqlcmd -S $Server -U $username -P $password -d $DatabaseName -i clean_data.sql
bcp $DatabaseName.dbo.league in $SqlSourceDataPath/league.csv -c -E -S $Server -U $username -P $password
bcp $DatabaseName.dbo.season in $SqlSourceDataPath/season.csv -c -E -S $Server -U $username -P $password
bcp $DatabaseName.dbo.team in $SqlSourceDataPath/team.csv -c -E -S $Server -U $username -P $password
bcp $DatabaseName.dbo.player in $SqlSourceDataPath/player.csv -c -S $Server -U $username -P $password
bcp $DatabaseName.dbo.position in $SqlSourceDataPath/position.csv -c -S $Server -U $username -P $password
bcp $DatabaseName.dbo.contract in $SqlSourceDataPath/contract.csv -c -S $Server -U $username -P $password
