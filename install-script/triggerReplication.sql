create schema repl
GO

SELECT * INTO repl.contract
FROM dbo.contract
GO

SELECT * INTO repl.league
FROM dbo.league

GO
SELECT * INTO repl.player
FROM dbo.player

GO
SELECT * INTO repl.position
FROM dbo.position

GO 

SELECT * INTO repl.season
FROM dbo.season

go

SELECT * INTO repl.team
FROM dbo.team

GO

CREATE TRIGGER tContractTrigger on dbo.contract after insert, update, delete
as 

if exists (select * from inserted) and exists (select * from deleted)
	begin
		update r 
		set season_id = i.season_id,
			player_id = i.player_id,
			team_id = i.team_id,
			contract_amt = i.contract_amt
		from repl.contract r inner join inserted i
		on r.id = i.id
	end

if exists (select * from inserted) and not exists(select * from deleted)
	begin
		set identity_insert repl.contract on
		insert into repl.contract
		(id,season_id,player_id,team_id,contract_amt)
		select id, season_id, player_id, team_id, contract_amt
		from inserted 
		set identity_insert repl.contract off
	end

if not exists (select * from inserted) and exists(select * from deleted)
	begin
		delete r 
		from repl.contract r inner join deleted d
		on r.id = d.id
	end
go


CREATE TRIGGER tLeagueTrigger on dbo.league after insert, update, delete
as 

if exists (select * from inserted) and exists (select * from deleted)
	begin
		update r
		set league_name = i.league_name
		from repl.league r inner join inserted i on
		r.id = i.id
	end

if exists (select * from inserted) and not exists(select * from deleted)
	begin
		set identity_insert repl.league on
		insert into repl.league
		(id,league_name)
		select id, league_name from inserted
		set identity_insert repl.league off
	end

if not exists (select * from inserted) and exists(select * from deleted)
	begin
		delete r
		from repl.league r inner join deleted d
		on r.id = d.id
	end
go

CREATE TRIGGER tPlayerTrigger on dbo.player after insert, update, delete
as 
if exists (select * from inserted) and exists (select * from deleted)
	begin
		update r 
		set player_name = i.player_name,
		is_injured = i.is_injured
		from repl.player r inner join inserted i on
		r.id = i.id
	end

if exists (select * from inserted) and not exists(select * from deleted)
	begin
		set identity_insert repl.player on
		insert into repl.player
		(id,player_name,is_injured)
		select id, player_name, is_injured 
		from inserted
		set identity_insert repl.player off
	end

if not exists (select * from inserted) and exists(select * from deleted)
	begin
		delete r 
		from repl.player r inner join deleted d
		on r.id = d.id
	end
go

CREATE TRIGGER tPositionTrigger on dbo.position after insert, update, delete
as 
if exists (select * from inserted) and exists (select * from deleted)
	begin
		update r 
		set 
		position_name = i.position_name,
		player_id = i.player_id,
		pos = i.pos
		from repl.position r inner join inserted i
		on r.id = i.id
	end

if exists (select * from inserted) and not exists(select * from deleted)
	begin
		set identity_insert repl.position on
		insert into repl.position
		(id,position_name, player_id,pos)
		select id, position_name, player_id, pos
		from inserted
		set identity_insert repl.position off
	end

if not exists (select * from inserted) and exists(select * from deleted)
	begin
		delete r 
		from repl.position r inner join deleted d
		on r.id = d.id
	end
go

CREATE TRIGGER tseasonTrigger on dbo.season after insert, update, delete
as 
if exists (select * from inserted) and exists (select * from deleted)
	begin
		update  r 
		set 
		season_name = i.season_name,
		league_id = i.league_id,
		max_players = i.max_players
		from repl.season r inner join inserted i
		on r.id = i.id
	end

if exists (select * from inserted) and not exists(select * from deleted)
	begin
		set identity_insert repl.season on
		insert into repl.season
		(id,season_name,league_id,max_players)
		select id, season_name, league_id, max_players
		from inserted
		set identity_insert repl.season off
	end

if not exists (select * from inserted) and exists(select * from deleted)
	begin
		delete r 
		from repl.season r inner join deleted d on
		r.id = d.id
	end
go

CREATE TRIGGER tteamTrigger on dbo.team after insert, update, delete
as 
if exists (select * from inserted) and exists (select * from deleted)
	begin
		update r 
		set 
		team_name = i.team_name,
		team_city = i.team_city,
		league_id = i.league_id,
		max_contract_amt = i.max_contract_amt
		from repl.team r inner join inserted i
		on i.id = r.id
	end

if exists (select * from inserted) and not exists(select * from deleted)
	begin
		set identity_insert repl.team on
		insert into repl.team
		(id,team_name,team_city, league_id,max_contract_amt)
		select id, team_name,team_city, league_id, max_contract_amt
		from inserted
		set identity_insert repl.team off
	end

if not exists (select * from inserted) and exists(select * from deleted)
	begin
		delete r 
		from repl.team r inner join deleted d
		on r.id = d.id
	end
go

