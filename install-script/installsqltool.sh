/usr/bin/ruby -e “$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)”
brew tap microsoft/mssql-preview https://github.com/Microsoft/homebrew-mssql-preview
brew update

#for silent install ACCEPT_EULA=y 
export ACCEPT_EULA=y
#brew install –no-sandbox msodbcsql mssql-tools
brew install mssql-tools

