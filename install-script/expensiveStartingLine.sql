SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE proc [dbo].[sp_expensiveStartLine] 
@season as varchar(20) = '2000 Regular' 
AS

select player_name, pos2.position_name, team_name, c.contract_amt 
from 
player p inner join contract c on p.id = c.player_id
inner join team t on c.team_id = t.id 
inner join position pos2 ON pos2.player_id = p.id
inner join season s on s.id = c.season_id
inner join 
(select  pos1.position_name, c.team_id,max(c.contract_amt) as ContractAmt FROM
player p inner join contract c on p.id = c.player_id
inner join team t2 on t2.id = c.team_id 
INNER join position pos1 on p.id = pos1.player_id
group by pos1.position_name,team_id
) ag on ag.position_name = pos2.position_name
and ag.team_id = c.team_id
and c.contract_amt = ag.ContractAmt
where s.season_name = @season
order by team_name,contract_amt desc 


GO
