
create proc [dbo].[sp_removeInjured] @playerID int AS
BEGIN
update player
set is_injured = 0
where id = @playerID
end
