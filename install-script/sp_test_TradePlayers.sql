create proc sp_testTrade as
select team_id,player_id,player_name,c.contract_amt from contract c inner join player p on p.id = c.player_id
where team_id =17

declare @fromList as dbo.playerList
insert @fromList
values (428) -- Jordan Mickey


insert @fromList
values(466) -- Khris Middleton

insert @fromList
values(601) -- Otto Porter Jr.

declare @toList as dbo.playerList 
insert @toList
values(807) -- Zaza Pachulia
--values (428)
insert @toList
values(125) -- Chandler Parson

exec sp_tradePlayers 17, 18, @fromList, @toList 
