/*===============================================================
 File: drop_hoopDB.sql

 Summary:  Drops the Hoop DB sample database.  

 Date:  September 18, 2018
 Updated:  September 18, 2018

 ----------------------------------------------------------------

   This file is part of the Toptal Test Project.

   All data in this database is fictitious.

=================================================================*/

/* How to run this script:
 *
 * 1. Run sqlcmd on mac/linux or execute run.sh with the proper 
      connection string credentials.

   2. Copy this script and the install files to /Temp/ or set the 
      following environment variable to your own data path.
 */
 
/* 3. Append the SQL server version number to database name if you 
 *    want to differentiate it from other installs of hoopDB
 */

/* Execute the script
 */

IF '$(SqlSourceDataPath)' IS NULL OR '$(SqlSourceDataPath)' = ''
BEGIN
  RAISERROR(N'The variable SqlSourceDataPath must be defined.', 16, 127) with nowait
  RETURN
END;

SET NOCOUNT OFF;
GO

PRINT CONVERT(varchar(1000),@@VERSION);
GO

PRINT '';
PRINT 'Started - '+CONVERT(varchar,GETDATE(),121);
GO

USE [master];
GO

PRINT '';
PRINT '*** Dropping Database';
GO

IF EXISTS (SELECT [name] FROM [master].[sys].[databases] WHERE [name] = N'$(DatabaseName)')
    DROP DATABASE $(DatabaseName);

-- If the database has any other open connections close the network connection.
IF @@ERROR = 3702 
    RAISERROR('$(DatabaseName) database cannot be dropped because there are still other open connections', 127, 127) WITH NOWAIT, LOG;
GO

