/*===============================================================
 File: load_hoopDB.sql

 Summary:  Loads the sample Basketball League data into 
           HoopDB2 tables. 

 Date:  September 18, 2018
 Updated:  September 18, 2018

 ----------------------------------------------------------------

   This file is part of the Toptal Test Project.

   All data in this database is fictitious.

=================================================================*/


/* CHECK FOR DATABASE IF IT DOESN'T EXISTS, DO NOT RUN THE REST OF THE SCRIPT */
IF NOT EXISTS (SELECT TOP 1 1 FROM sys.databases WHERE name = N'$(DatabaseName)')
BEGIN
PRINT '*******************************************************************************************************************************************************************'
+char(10)+'********$(DatabaseName) Database does not exist.  Make sure that the script is being run in SQLCMD mode and that the variables have been correctly set.*********'
+char(10)+'*******************************************************************************************************************************************************************';
SET NOEXEC ON;
END
GO

-- ******************************************************
-- Clean data
-- ******************************************************
/*alter table dbo.season nocheck constraint [FK_season_league]
go
alter table dbo.team nocheck constraint [FK_team_league]
go
truncate table dbo.league
go
alter table dbo.season with check check constraint [FK_season_league]
alter table dbo.team with check check constraint [FK_team_league]
go
*/
SELECT 'ALTER TABLE [' + s.name + '].[' + o.name + '] WITH CHECK CHECK CONSTRAINT [' + i.name + ']'
FROM sys.foreign_keys i
INNER JOIN sys.objects o ON i.parent_object_id = o.OBJECT_ID
INNER JOIN sys.schemas s ON o.schema_id = s.schema_id
go
delete dbo.season
delete dbo.team
delete dbo.player
delete dbo.position
delete dbo.contract
delete dbo.league
GO
SELECT 'ALTER TABLE [' + s.name + '].[' + o.name + '] NOCHECK CONSTRAINT all'
FROM sys.foreign_keys i
INNER JOIN sys.objects o ON i.parent_object_id = o.OBJECT_ID
INNER JOIN sys.schemas s ON o.schema_id = s.schema_id

/*
truncate table dbo.season

truncate table dbo.team
truncate table dbo.player
truncate table dbo.position
truncate table dbo.contract
*/
