SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

create proc [dbo].[sp_monthlyLuxuryTaxrpt] 
@season varchar(20)='2000 Regular'
as
select t.team_name, t.max_contract_amt, sum(contract_amt) as allocatedAmount, t.max_contract_amt - sum(contract_amt) as luxuryTax
from team t inner join contract c on t.id = c.team_id
inner join season s on s.id = c.season_id
where c.player_id not in (select distinct id from player p where p.is_injured = 1)
and s.season_name = @season
group by t.max_contract_amt, t.team_name
having sum(contract_amt) > max_contract_amt
GO
