
create proc [dbo].[sp_addInjured] @playerID int AS
BEGIN
update player
set is_injured = 1
where id = @playerID
end
