SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE proc [dbo].[sp_tradePlayers]
 @fromTeam int, 
 @toTeam int,
 @fromplayerList playerList READONLY,
 @toplayerList playerList READONLY,
 @season as varchar(20) = '2000 Regular' 
 AS
BEGIN

declare @totalFromContract as numeric(18,0)

select @totalFromContract = 
sum(c.contract_amt) FROM
contract c inner join season s on c.season_id = s.id
where s.season_name = @season
and c.player_id in (select playerId from @fromPlayerList)
and c.team_id = @fromTeam

declare @totalToContract as numeric(18,0)

select @totalToContract = sum(c.contract_amt) from 
contract c inner join season s on c.season_id = s.id
where s.season_name = @season
and c.player_id in (select playerId from @toPlayerList)
and c.team_id = @toTeam

declare @threshold as float 
set @threshold = 0.2

if (ABS(@totalToContract - @totalFromContract)/@totalFromContract > @threshold) 
BEGIN
    RAISERROR(N'The total trade contract value must be within the threshold value of 20%',10,1 )
END
ELSE
BEGIN
-- check if there are enough spots in the @fromTeam
    declare @fromRoster as int 
    select @fromRoster = count(1) from 
    contract c inner join season s on c.season_id = s.id
    where s.season_name = @season
    and c.team_id = @fromTeam

    declare @fromAddedRoster as INT
    select @fromAddedRoster = count(1) from 
    @fromplayerList

    declare @toAddedRoster as INT
    select @toAddedRoster = count(1) FROM
    @toplayerList


    declare @maxRoster as INT
    select @maxRoster = max_players from season s
    inner join team t on s.league_id = t.league_id
    where t.id = @fromTeam
    and s.season_name = @season

    declare @emptyFromSpots as int 
 /*   print 'Max # of people '+cast(@maxRoster as varchar(20))
    print 'New Players to come in '+cast(@toAddedRoster as varchar(20))
    print 'Players to leave '+cast(@fromAddedRoster as varchar(20))
    print 'Current Roster '+cast(@fromRoster as varchar(20))
*/
    set @emptyFromSpots = @maxRoster - @fromRoster + @fromAddedRoster
  /*  print 'Available spots '+cast(@emptyFromSpots as varchar(20))
    print 'New players to come in '+cast(@toAddedRoster as varchar(20))
*/
    if (@emptyFromSpots - @toAddedRoster  < 0 )
    BEGIN 
        RAISERROR(N'There are not enough empty spots to trade from',10,1)
    END
    
    ELSE
    BEGIN
    -- check if there are enough spots in the @toTeam 
        declare @toRoster as INT
        select @toRoster = count(1) from 
        contract c inner join season s on c.season_id = s.id
        where s.season_name = @season
        and c.team_id = @toTeam

        declare @emptyToSpots as INT
        set @emptyFromSpots = @maxRoster- @toRoster + @toAddedRoster 

        if (@emptyToSpots - @fromAddedRoster < 0)
        BEGIN
            RAISERROR(N'There are not enough empty spots to trade to',10,1)
        END
        ELSE
        BEGIN
        -- all checks ok make the swap

          update contract
          SET
          team_id = @toTeam
           WHERE
           team_id = @fromTeam AND
           player_id in (select playerId from @fromplayerList)

           update contract
            set team_id = @fromTeam 
            where team_id = @toTeam AND
           player_id in (select playerId from @toPlayerList)

          end
    END
END
END
GO
