/*===============================================================
 File: create_tables.sql

 Summary:  Creates the Basketball League management tables into 
           HoopDB2 database. 

 Date:  September 18, 2018
 Updated:  September 18, 2018

 ----------------------------------------------------------------

   This file is part of the Toptal Test Project.

   All data in this database is fictitious.

=================================================================*/


/* CHECK FOR DATABASE IF IT DOESN'T EXISTS, DO NOT RUN THE REST OF THE SCRIPT */
IF NOT EXISTS (SELECT TOP 1 1 FROM sys.databases WHERE name = N'$(DatabaseName)')
BEGIN
PRINT '*******************************************************************************************************************************************************************'
+char(10)+'********$(DatabaseName) Database does not exist.  Make sure that the script is being run in SQLCMD mode and that the variables have been correctly set.*********'
+char(10)+'*******************************************************************************************************************************************************************';
SET NOEXEC ON;
END
GO

USE $(DatabaseName);
GO


create type playerList as TABLE
(playerId int)
go

create proc [dbo].[sp_addInjured] @playerID int AS
BEGIN
update player
set is_injured = 1
where id = @playerID
end
GO

create proc sp_ExceededContractAmt
@season as varchar(20) = '$(DefaultSeason)' as
SELECT t.team_name,t.max_contract_amt,sum(c.contract_amt) as contract_amt FROM
team t inner join contract c 
on t.id = c.team_id 
inner join season s on s.id = c.season_id
where s.season_name = @season
group by t.team_name, t.max_contract_amt
having sum(c.contract_amt) > (t.max_contract_amt)

GO

CREATE proc [dbo].[sp_expensiveStartLine] 
@season as varchar(20) = '$(DefaultSeason)' 
AS

select player_name, pos2.position_name, team_name, c.contract_amt 
from 
player p inner join contract c on p.id = c.player_id
inner join team t on c.team_id = t.id 
inner join position pos2 ON pos2.player_id = p.id
inner join season s on s.id = c.season_id
inner join 
(select  pos1.position_name, c.team_id,max(c.contract_amt) as ContractAmt FROM
player p inner join contract c on p.id = c.player_id
inner join team t2 on t2.id = c.team_id 
INNER join position pos1 on p.id = pos1.player_id
group by pos1.position_name,team_id
) ag on ag.position_name = pos2.position_name
and ag.team_id = c.team_id
and c.contract_amt = ag.ContractAmt
where s.season_name = @season
order by team_name,contract_amt desc 


GO

create proc [dbo].[sp_monthlyLuxuryTaxrpt] 
@season varchar(20)='$(DefaultSeason)'
as
select t.team_name, t.max_contract_amt, sum(contract_amt) as allocatedAmount, t.max_contract_amt - sum(contract_amt) as luxuryTax
from team t inner join contract c on t.id = c.team_id
inner join season s on s.id = c.season_id
where c.player_id not in (select distinct id from player p where p.is_injured = 1)
and s.season_name = @season
group by t.max_contract_amt, t.team_name
having sum(contract_amt) > max_contract_amt
GO

CREATE proc [dbo].[sp_mostExpensivePlayer] 
@season varchar(20) = '$(DefaultSeason)' as
select top 1 p.player_name, contract_amt 
from contract c 
inner join player p on c.player_id = p.id 
inner join season s on s.id = c.season_id
order by contract_amt desc
GO

create proc [dbo].[sp_mostExpensiveTeam] 
@season varchar(20) = '$(DefaultSeason)'
as
select top 1 t.team_city, t.team_name,max(allocated) as Spent 
from 
(
select team_id, sum(contract_amt) allocated
from contract c inner join
season s on s.id = c.season_id
where s.season_name = @season 
group by team_id
) a inner join team t on a.team_id = t.id 
group by t.team_name , t.team_city
order by max(allocated) desc 
GO

create proc [dbo].[sp_removeInjured] @playerID int AS
BEGIN
update player
set is_injured = 0
where id = @playerID
end
GO

CREATE proc [dbo].[sp_tradePlayers]
 @fromTeam int, 
 @toTeam int,
 @fromplayerList playerList READONLY,
 @toplayerList playerList READONLY,
 @season as varchar(20) = '2000 Regular' 
 AS
BEGIN
-- check if within the 20% threshold
declare @totalFromContract as numeric(18,0)

select @totalFromContract = 
sum(c.contract_amt) FROM
contract c inner join season s on c.season_id = s.id
where s.season_name = @season
and c.player_id in (select playerId from @fromPlayerList)
and c.team_id = @fromTeam

declare @totalToContract as numeric(18,0)

select @totalToContract = sum(c.contract_amt) from 
contract c inner join season s on c.season_id = s.id
where s.season_name = @season
and c.player_id in (select playerId from @toPlayerList)
and c.team_id = @toTeam

declare @threshold as float 
set @threshold = 0.2

if (ABS(@totalToContract - @totalFromContract)/@totalFromContract < @threshold) 
BEGIN
    RAISERROR(N'The total trade contract value must be within the threshold value of 20%%',10,1 )
END
ELSE
BEGIN
-- check if there are enough spots in the @fromTeam
    declare @fromRoster as int 
    select @fromRoster = count(1) from 
    contract c inner join season s on c.season_id = s.id
    where s.season_name = @season
    and c.team_id = @fromTeam

    declare @fromAddedRoster as INT
    select @fromAddedRoster = count(1) from 
    @fromplayerList

    declare @toAddedRoster as INT
    select @toAddedRoster = count(1) FROM
    @toplayerList


    declare @maxRoster as INT
    select @maxRoster = max_players from season s
    inner join team t on s.league_id = t.league_id
    where t.id = @fromTeam
    and s.season_name = @season

    declare @emptyFromSpots as int 
 /*   print 'Max # of people '+cast(@maxRoster as varchar(20))
    print 'New Players to come in '+cast(@toAddedRoster as varchar(20))
    print 'Players to leave '+cast(@fromAddedRoster as varchar(20))
    print 'Current Roster '+cast(@fromRoster as varchar(20))
*/
    set @emptyFromSpots = @maxRoster - @fromRoster + @fromAddedRoster
  /*  print 'Available spots '+cast(@emptyFromSpots as varchar(20))
    print 'New players to come in '+cast(@toAddedRoster as varchar(20))
*/
    if (@emptyFromSpots - @toAddedRoster  < 0 )
    BEGIN 
        RAISERROR(N'There are not enough empty spots to trade from',10,1)
    END
    
    ELSE
    BEGIN
    -- check if there are enough spots in the @toTeam 
        declare @toRoster as INT
        select @toRoster = count(1) from 
        contract c inner join season s on c.season_id = s.id
        where s.season_name = @season
        and c.team_id = @toTeam

        declare @emptyToSpots as INT
        set @emptyFromSpots = @maxRoster- @toRoster + @toAddedRoster 

        if (@emptyToSpots - @fromAddedRoster < 0)
        BEGIN
            RAISERROR(N'There are not enough empty spots to trade to',10,1)
        END
        ELSE
        BEGIN
        -- all checks ok make the swap

          update contract
          SET
          team_id = @toTeam
           WHERE
           team_id = @fromTeam AND
           player_id in (select playerId from @fromplayerList)

           update contract
            set team_id = @fromTeam 
            where team_id = @toTeam AND
           player_id in (select playerId from @toPlayerList)

          end
    END
END
END
GO
create proc sp_testTrade as
select team_id,player_id,player_name,c.contract_amt from contract c inner join player p on p.id = c.player_id
where team_id =18
declare @fromList as dbo.playerList
insert @fromList
values (428) -- Jordan Mickey


insert @fromList
values(466) -- Khris Middleton

insert @fromList
values(601) -- Otto Porter Jr.

declare @toList as dbo.playerList 
insert @toList
values(808) -- Willie Reed
insert @toList
values(125) -- Chandler Parson

exec sp_tradePlayers 18, 17, @fromList, @toList 
go
