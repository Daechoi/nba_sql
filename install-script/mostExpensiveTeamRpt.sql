SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

create proc [dbo].[sp_mostExpensiveTeam] 
@season varchar(20) = '2000 Regular'
as
select top 1 t.team_city, t.team_name,max(allocated) as Spent 
from 
(
select team_id, sum(contract_amt) allocated
from contract c inner join
season s on s.id = c.season_id
where s.season_name = @season 
group by team_id
) a inner join team t on a.team_id = t.id 
group by t.team_name , t.team_city
order by max(allocated) desc 
GO
