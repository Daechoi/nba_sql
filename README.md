# Basketball League Management

This project implements MSSQL database to support storing information about the league, teams, player and their contracts and provides reporting functionality.

![ERM](http://git.toptal.com/Dae-Y-Choi/dae-y-choi/raw/master/BasketballERM.png)

## The Data model represents the following entities and relationships:

- Each league can have multiple seasons.
- Each league contains N teams playing in it.
- Each team can have 15 players on the team. However, it is possible that a league can decide each season what is the maximum number of players.
- Each player can play on one or multiple positions: point guard, shooting guard, small forward, power forward and a center.
- Each player has a contract with a team for a specific season. It is possible to have a one-year contract or multi-year contract.

## Entity Constraints & Rules:

- Each team can spend a limited budget for his team. It means if a salary budget is $50.000.000, overall contract value should be below that value. Otherwise, a league will detect the limit was breached and a team will have to pay for a luxury tax, which is 100% of an value above the limit. League checks contract budgets.

- Teams can trade players. It is important to know when teams are doing trades, a sum of player’s contracts on each side must be similar. There can be a difference of 20% of overall traded value.
-- E.g. Player A and B have an annual contract value of $10.000.000 and a team wants to trade them in order to get a player C. His annual contract value is $12.000.000. This is allowed, however, teams must be sure they have enough empty spots on a roster.
-- Players can get injured during a season. In that case, their contract is not calculated in a budget. Also, in that case, an empty spot is available on a team roster.

## Getting Started

### Pre-requisite
- Docker daemon is running
- Internet is reachable to download latest Microsoft SQL Server docker image.
- installsqltool.sh is invoked to install necessary client applications for MSSQL
- Bash terminal to run scripts.
- Project was tested on the **Mac** environment.  For **Windows** environment, directory syntax to .env variable will need to be tweaked.

### Deployment Procedure
1. Copy the file "env.sample" to "".env"
>cp env.sample .env

2.  Setup the environment variable in the .env file.
>Server=localhost
>username=sa
>password=
>SqlSourceDataPath=sample_data/
>DatabaseName=HoopDB
>DefaultSeason="2000 Regular"

Default Season will be set as the parameter in the stored procedures if it is not explicitly set with the value.

3. Install and spawn MSSQL server.
>dockerup.sh

4. Create the tables and stored procedures.
>create_db.sh

5. ** OPTIONAL ** Populate the sample data for a season.
>load_sample_data.sh

6. Setup the trigger based replication of data
>create_replication.sh

## Managing the league 

- Add player to the injured list *addInjuredPlayer.sql*
>exec dbo.sp_addInjured 4

- Remove the player from the injured list *removeInjuredPlayer.sql*
>exec dbo.sp_removeInjured 4

- Trade between two teams. Allow trading multiple players from each side.  *UDT_playerList.sql*, *tradePlayers.sql*
Setup the list parameters for multiple players from each side
>declare @fromTeam as int
>set @fromTeam = 14
>declare @fromPlayerList as playerList
>insert @fromPlayerList
>(playerId)
>select player_ID 
>from dbo.contract 
>where team_id = 14
>and season_id=18
>and contract_amt < 1000000
>declare @toTeam as int
>set @toTeam = 15
>declare @toPlayerList as playerList
>insert @toPlayerList
>(playerId)
>select player_ID 
>from dbo.contract 
>where team_id = 15
>and season_id=18
>and contract_amt < 1000000
>exec dbo.sp_tradePlayers @fromTeam, @toTeam, @fromPlayerList, @toPlayerList

## Running the reports

- List the most expensive starting lineup for a specific team. A starting lineup has one player on each position and it has to return five players, one for each position.  *expensiveStartingLine.sql*
>exec dbo.sp_expensiveStartLine

- List of the teams that have breached a contract limit, include the luxury tax record.  *monthlyLuxuryTaxrpt.sql*
>exec dbo.sp_monthlyLuxuryTaxrpt

- List the teams that went over the budget limit for during the season. *exceedContractAmt.sql*
>exec dbo.sp_ExceededContractAmt

- Get the most expensive team *mostExpensiveTeamRpt.sql*
>exec dbo.sp_mostExpensiveteam

- Get most expensive player. *mostExpensivePlayerRpt*
>exec dbo.sp_mostExpensivePlayer

## Maintenance
- Backup of the database. *backup_data.sh*
Setup the correct deployed directory and replace the correct username in *create_backup_job.sh*
>create_backup_job.sh

This sets up the cron job to execute backup_data.sh every 2 hours for backup

- Replication database on the same machine.  *triggerReplication.sql*  Read-only data is replicated with corresponding **"repl"** schema.
  Replication is based on the trigger of source table
>create_replication.sh
	
- **DatabaseLog** table keeps track of all the changes to data model.

- **ErrorLog** table maintains all error status in the database.

## Author

Dae Choi
daechoi@outlook.com

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Kudos to ESPN.com for the sample player and team data
